/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';
import AppNavigator from './src/Navigator/AppNavigator';
// import {BackHardware} from './src/Navigator/MainTabNavigator';
import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['Remote debugger']);

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  render() {
    return (
      <View style={styles.container}>
        <AppNavigator />
      </View>
    );
  }
}

// const App: () => React$Node = () => {
//   return (
//     <View>
//       <Text>Hello123</Text>
//       <AppNavigator></AppNavigator>
//     </View>
//   );
// };
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

// export default App;
