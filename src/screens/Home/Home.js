import React, {Component} from 'react';
import {View, StyleSheet, Text, Button} from 'react-native';

export default class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          onPress={async () => {
            this.props.navigation.navigate('Scanner');
          }}
          title="Scanner"></Button>

        <Button
          onPress={async () => {
            this.props.navigation.navigate('AIChatBot');
          }}
          title="AIChatBot"></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});
