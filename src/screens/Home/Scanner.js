import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  InteractionManager,
  Image,
  ActivityIndicator,
} from 'react-native';
import axios from 'react-native-axios';
import Shimmer from 'react-native-shimmer';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import FastImage from 'react-native-fast-image';

export default class Scanner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: '',
      isFetched: false,
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      axios.get(`https://jsonplaceholder.typicode.com/photos`).then(res => {
        const userData = res.data;
        this.setState({users: userData, isFetched: true});
      });
    });
  }

  renderItem = ({item}) => (
    <View key={item.id}>
      <FastImage
        style={{width: 200, height: 200}}
        source={{
          uri: item.thumbnailUrl,
          priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text>{item.title}</Text>
    </View>
  );

  render() {
    console.log('users', this.state.users);
    return (
      <View style={styles.container}>
        <ShimmerPlaceHolder visible={this.state.isFetched} />
        <FlatList
          data={this.state.users}
          renderItem={this.renderItem}
          keyExtractor={item => item.id.toString()}
          //   ListEmptyComponent={
          //     <ShimmerPlaceHolder autoRun={true} visible={this.state.isFetched} />
          //   }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
