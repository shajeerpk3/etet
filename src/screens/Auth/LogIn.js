import React, {Component} from 'react';
import {Alert, View, Text, Button, StyleSheet, TextInput} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class Login extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({navigation}) => {
    return {};
  };

  onPress() {
    console.log('hihihi');
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Text style={{color: 'white', fontSize: 30, padding: 20}}>Login</Text>

        <View
          style={{
            borderBottomColor: 'white',
            borderColor: '#0000',
            borderWidth: 1,
            marginLeft: 20,
            marginRight: 20,
            width: '80%',
          }}>
          <TextInput
            placeholder="Username"
            placeholderTextColor="white"
            style={{color: 'white'}}></TextInput>
        </View>

        <View
          style={{
            borderBottomColor: 'white',
            borderColor: '#0000',
            borderWidth: 1,
            marginLeft: 20,
            marginRight: 20,
            width: '80%',
          }}>
          <TextInput
            placeholderTextColor="white"
            placeholder="password"
            autoCompleteType="password"
            style={{color: 'white'}}></TextInput>
        </View>

        <View
          style={{
            borderWidth: 1,
            marginLeft: 20,
            marginRight: 20,
            width: '80%',
            marginTop: 20,
          }}>
          <Button
            onPress={async () => {
              await AsyncStorage.setItem('userToken', '123'),
                this.props.navigation.navigate('Main');
            }}
            title="LOGIN"></Button>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#69807a',
  },
});
