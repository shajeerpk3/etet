import React, {Component} from 'react';
import {View, Button} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class Settings extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <View
          style={{
            borderWidth: 1,
            marginLeft: 20,
            marginRight: 20,
            width: '80%',
            marginTop: 20,
          }}>
          <Button
            onPress={async () => {
              AsyncStorage.clear();
              this.props.navigation.navigate('Auth');
            }}
            title="LOGOUT"></Button>
        </View>
      </View>
    );
  }
}
