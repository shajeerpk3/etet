import React, {Component} from 'react';
import {StyleSheet, View, FlatList, TouchableOpacity, Text} from 'react-native';

export default class SelectableItem extends React.Component {
  constructor() {
    super();

    this.handleOnPress = this.handleOnPress.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    const {isSelected} = this.props;
    return isSelected !== nextProps.isSelected;
  }

  handleOnPress() {
    const {onPress} = this.props;
    onPress();
  }

  render() {
    const {isSelected, text} = this.props;
    const textColor = isSelected ? 'blue' : 'black';

    return (
      <TouchableOpacity onPress={this.handleOnPress}>
        <View>
          <Text style={{color: textColor}}>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}
