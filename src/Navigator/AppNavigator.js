import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {ActivityIndicator, StatusBar, StyleSheet, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

import AsyncStorage from '@react-native-community/async-storage';

import LogIn from '../screens/Auth/LogIn';
import Home from '../screens/Home/Home';
import Scanner from '../screens/Home/Scanner';
import AIChatBot from '../screens/Home/AIChatBot';
import Chat from '../screens/Chat/Chat';
import Contact from '../screens/Contact/Contact';
import Vault from '../screens/Vault/Vault';
import Settings from '../screens/Settings/Settings';

const LogInStack = createStackNavigator(
  {
    LogIn: LogIn,
  },
  {headerMode: 'none'},
);

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: () => ({
        title: `Home`,
        headerBackTitle: null,
      }),
    },
    Scanner: {
      screen: Scanner,
      navigationOptions: () => ({
        title: `Scanner`,
        headerBackTitle: null,
      }),
    },
    AIChatBot: {
      screen: AIChatBot,
      navigationOptions: () => ({
        title: `AI ChatBot`,
        headerBackTitle: null,
      }),
    },
  },

  {
    headerLayoutPreset: 'left',
    navigationOptions: {
      tabBarLabel: 'Home',
    },
  },
);

const ContactStack = createStackNavigator(
  {
    Contact: {
      screen: Contact,
      // path: 'etet/:homepage',
      navigationOptions: () => ({
        title: `Contact`,
        headerBackTitle: null,
      }),
    },
  },
  {
    headerLayoutPreset: 'left',
    navigationOptions: {
      tabBarLabel: 'Contact',
    },
  },
);

const ChatStack = createStackNavigator(
  {
    Chat: {
      screen: Chat,
      // path: 'etet/:homepage',
      navigationOptions: () => ({
        title: `Chat`,
        headerBackTitle: null,
      }),
    },
  },
  {
    headerLayoutPreset: 'left',
    navigationOptions: {
      tabBarLabel: 'Chat',
    },
  },
);

const VaultStack = createStackNavigator(
  {
    Vault: {
      screen: Vault,
      // path: 'etet/:homepage',
      navigationOptions: () => ({
        title: `Vault`,
        headerBackTitle: null,
      }),
    },
  },
  {
    headerLayoutPreset: 'left',
    navigationOptions: {
      tabBarLabel: 'Vault',
    },
  },
);

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: Settings,
      navigationOptions: () => ({
        title: `Settings`,
        headerBackTitle: null,
      }),
    },
  },
  {
    headerLayoutPreset: 'left',
    navigationOptions: {
      tabBarLabel: 'Settings',
    },
  },
);

const AppStack = createBottomTabNavigator(
  {
    HomeStack,
    ContactStack,
    VaultStack,
    ChatStack,
    SettingsStack,
  },
  {
    defaultNavigationOptions: ({navigation, screenProps}) => ({
      tabBarLabel: 'Home',
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName = '';
        if (routeName === 'HomeStack') {
          iconName = `home`;
        } else if (routeName === 'ContactStack') {
          iconName = `contacts`;
        } else if (routeName === 'VaultStack') {
          iconName = `addfile`;
        } else if (routeName === 'ChatStack') {
          iconName = `message1`;
        } else if (routeName === 'SettingsStack') {
          iconName = `setting`;
        }

        return (
          <AntDesign name={iconName} backgroundColor="#3b5998" size={20} />
        );
      },
    }),
    tabBarOptions: {
      activeTintColor: '#3370FF',
      inactiveTintColor: '#616161',

      inactiveBackgroundColor: '#fff',
      labelStyle: {
        fontSize: 14,
        fontFamily: 'Roboto',
      },
      style: {
        backgroundColor: '#fff',
        borderTopWidth: 0.5,
        height: 55,
        shadowOffset: {width: 5, height: 3},
        shadowColor: 'black',
        shadowOpacity: 0.5,
      },
    },
  },
);

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    console.log('userTokenuserTokenuserToken', userToken);
    this.props.navigation.navigate(!userToken ? 'Auth' : 'Main');
  };

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      Main: AppStack,
      Auth: LogInStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);
